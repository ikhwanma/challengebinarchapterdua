# Challenge Binar Chapter Dua

Membuat layout GoJek dan Shopee

## Layout GoJek
<img src="/uploads/73bc024aa22a222a99325c5a58c9b73a/gojek__1_.png" width="200">
<img src="/uploads/440c217e7c193a20a8ac3f7dcf8adcae/gojek__2_.png" width="200">
<img src="/uploads/a4735465740bf196c7d63d94b45f80a9/gojek__3_.png" width="200">
<img src="/uploads/95b9a751bcb5b13b54b9d10981053aa2/gojek__4_.png" width="200">

## Layout Shopee
<img src="/uploads/0a63c6abb1defd29c3907d6243defdbf/shopee__1_.png" width="200">
<img src="/uploads/a4608acf8e1e6acc635a00ffc5c7bd5c/shopee__2_.png" width="200">
<img src="/uploads/eb5be79c4413914c0cb95bc09e4d1c85/shopee__3_.png" width="200">
<img src="/uploads/3071b175c275462a66d4619e2c6acf84/shopee__4_.png" width="200">

